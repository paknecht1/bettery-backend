package com.ds1.bettery.service;

import com.ds1.bettery.domain.Bet;
import com.ds1.bettery.domain.BetteryUser;
import com.ds1.bettery.domain.Role;
import com.ds1.bettery.exception.CustomException;
import com.ds1.bettery.exception.CustomValidationException;
import com.ds1.bettery.respository.UserRepository;
import com.ds1.bettery.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UserService {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private JwtTokenProvider jwtTokenProvider;

    private AuthenticationManager authenticationManager;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
    }

    public String login(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public String register(BetteryUser betteryUser) {
        validateUser(betteryUser);
        betteryUser.setPassword(passwordEncoder.encode(betteryUser.getPassword()));
        userRepository.save(betteryUser);
        return jwtTokenProvider.createToken(betteryUser.getUsername(), betteryUser.getRoles());
    }

    private void validateUser(BetteryUser betteryUser) {
        List<FieldError> possibleErrors = new ArrayList<>();
        if (userRepository.existsByUsername(betteryUser.getUsername())) {
            possibleErrors.add(new FieldError("username", "username", "username isalready taken"));
        } else if (userRepository.existsByEmail(betteryUser.getEmail())) {
            possibleErrors.add(new FieldError("email", "email", "email isalready taken"));
        }
        if (!possibleErrors.isEmpty()) {
            throw new CustomValidationException("Validation Failed", HttpStatus.UNPROCESSABLE_ENTITY, possibleErrors);
        }
    }

    @Transactional
    public Collection<Bet> getBetsOfUser(String currentUser) {
        BetteryUser userWithBet = userRepository.findByUsername(currentUser);
        if (!userWithBet.getBets().isEmpty()) {
            return userWithBet.getBets();
        }
        return Collections.emptyList();
    }

    public BetteryUser search(String username) {
        BetteryUser user = userRepository.findByUsername(username);
        if (user == null) {
            throw new CustomException("The user dosen't exists", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    public String getUsernameFromCurrentUser() {
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal.getUsername();
    }

    public BetteryUser getCurrentUser() {
        String username = getUsernameFromCurrentUser();
        return search(username);
    }

    public Collection<BetteryUser> getAdmins() {
        List<BetteryUser> admins = new ArrayList<>();
        Iterator<BetteryUser> betteryUserIterator = userRepository.findAll().iterator();
        while (betteryUserIterator.hasNext()) {
            BetteryUser betteryUser = betteryUserIterator.next();
            if (betteryUser.getRoles().contains(Role.ROLE_ADMIN)) {
                admins.add(betteryUser);
            }
        }
        return admins;
    }


    public void deleteUserByName(String username) {
        userRepository.deleteByUsername(username);
    }

    public BetteryUser whoami(HttpServletRequest req) {
        return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
    }

    public String refresh(String username) {
        return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
    }

    public void setBalanceAfterJoiningBet(BetteryUser betteryUser, double amount) {

        if(betteryUser.getBalance()>=amount) {
            betteryUser.setBalance(betteryUser.getBalance() - amount);
            userRepository.save(betteryUser);
        } else {
            throw new CustomException("Joinee has insufficient funds to join bet", HttpStatus.FORBIDDEN);
        }
    }

    public void updateBalanceForBetCreator(BetteryUser betteryUser, double betterAmount, int numberOfParticipants){
        double amountToTransferToBetPot = betterAmount*numberOfParticipants;
        if(amountToTransferToBetPot<=betteryUser.getBalance()){
            betteryUser.setBalance(betteryUser.getBalance()-amountToTransferToBetPot);
            userRepository.save(betteryUser);
        } else {
            throw new CustomException("Creator cannot create bet because of insufficient funds", HttpStatus.FORBIDDEN);
        }
    }

    public void setBalanceForUserAfterWin(BetteryUser betteryUser, double amount){
        betteryUser.setBalance(betteryUser.getBalance() + amount);
        userRepository.save(betteryUser);
    }

}
