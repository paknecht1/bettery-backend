package com.ds1.bettery.service;

import com.ds1.bettery.domain.Bet;
import com.ds1.bettery.domain.BetPot;
import com.ds1.bettery.domain.BetteryUser;
import com.ds1.bettery.dto.BetDTO;
import com.ds1.bettery.dto.BetteryUserDTO;
import com.ds1.bettery.dto.mappers.BetMapper;
import com.ds1.bettery.dto.mappers.UserMapper;
import com.ds1.bettery.exception.CustomException;
import com.ds1.bettery.respository.BetPotRepository;
import com.ds1.bettery.respository.BetRepository;
import com.ds1.bettery.respository.UserRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BetService {

    private BetRepository betRepository;
    private BetPotRepository betPotRepository;
    private UserService userService;
    private UserRepository userRepository;

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private BetMapper betMapper = Mappers.getMapper(BetMapper.class);


    @Autowired
    public BetService(BetRepository betRepository, BetPotRepository betPotRepository, UserService userService, UserRepository userRepository) {
        this.betRepository = betRepository;
        this.betPotRepository = betPotRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Transactional
    public Optional<Bet> getBetById(long betID) {
        Optional<Bet> bet = betRepository.findById(betID);
        return bet;
    }

    @Transactional
    public BetDTO getBetDTOById(long betID) {
        Bet bet = betRepository.findById(betID).get();
        BetDTO betDTO = betMapper.mapBetToBetDTO(bet);
        return betDTO;
    }


    @Transactional
    public List<Bet> getAllBets() {
        return betRepository.findAll();
    }

    @Transactional
    public Collection<BetDTO> getAllBetsWithJoinableField() {
        BetteryUser currentUser = userService.getCurrentUser();
        List<Bet> bets = getAllBets();
        Collection<BetDTO> betDTOS = betMapper.mapBetListToBetDTOList(bets);
        for (BetDTO betDTO : betDTOS) {
            betDTO.setCanUserJoin(!isUserPartOfBet(betDTO.getBetId(), currentUser.getUserId()));
        }
        return betDTOS;
    }

    @Transactional
    public void deleteById(long betId) {
        Optional<Bet> betToDelete = betRepository.findById(betId);
        if (betToDelete.isPresent()) {
            Bet bet = betToDelete.get();
            BetteryUser betteryUser = bet.getCreator();
            BetteryUser currentUser = userService.getCurrentUser();
            if (betteryUser.equals(currentUser) || userService.getAdmins().contains(currentUser)) {
                betRepository.delete(bet);
            } else {
                throw new CustomException("User not authorized to delete the Bet", HttpStatus.UNAUTHORIZED);
            }
        } else {
            throw new CustomException("Bet to delete doesn't exist", HttpStatus.NOT_FOUND);
        }
    }

    @Transactional
    public Collection<BetteryUserDTO> getJoineesForBet(long betID) {
        BetPot betPot = betRepository.findById(betID).get().getBetPot();
        return userMapper.mapUserListToDTOList(betPot.getUsers());
    }

    @Transactional
    public void currentUserJoinsBet(long betID) {
        Optional<Bet> bet = getBetById(betID);
        BetteryUser betteryUser = userService.getCurrentUser();
        if (bet.isPresent()) {
            Bet betToJoin = bet.get();
            BetPot betPot = betRepository.findById(betID).get().getBetPot();
            Collection<BetteryUser> users = betPot.getUsers();
            if (betToJoin.getNumberOfParticipants() > users.size()) {
                if (!users.contains(betteryUser)) {
                    users.add(betteryUser);
                    userService.setBalanceAfterJoiningBet(betteryUser, betToJoin.getJoinerAmount());
                    betPot.setAmount(betPot.getAmount() + betToJoin.getJoinerAmount());
                    betPotRepository.save(betPot);
                } else {
                    throw new CustomException("User already part of bet", HttpStatus.ALREADY_REPORTED);
                }
            } else {
                throw new CustomException("Bet already reached max number of participants", HttpStatus.FORBIDDEN);
            }
        } else {
            throw new CustomException("Bet that user wants to join doesn't exist", HttpStatus.NOT_FOUND);
        }
    }

    //for dev purposes
    @Transactional
    public void userJoinsBet(long betID, BetteryUser betteryUser) {
        Optional<Bet> bet = getBetById(betID);
        if (bet.isPresent()) {
            Bet betToJoin = bet.get();
            BetPot betPot = betRepository.findById(betID).get().getBetPot();
            Collection<BetteryUser> users = betPot.getUsers();
            if (betToJoin.getNumberOfParticipants() > users.size()) {
                if (!users.contains(betteryUser)) {
                    users.add(betteryUser);
                    userService.setBalanceAfterJoiningBet(betteryUser, betToJoin.getJoinerAmount());
                    betPot.setAmount(betPot.getAmount() + betToJoin.getJoinerAmount());
                    betPotRepository.save(betPot);
                } else {
                    throw new CustomException("User already part of bet", HttpStatus.ALREADY_REPORTED);
                }
            } else {
                throw new CustomException("Bet already reached max number of participants", HttpStatus.FORBIDDEN);
            }
        } else {
            throw new CustomException("Bet that user wants to join doesn't exist", HttpStatus.NOT_FOUND);
        }
    }

    @Transactional
    public Bet currentUserCreatesBet(BetDTO betDTO) {
        Bet bet = betMapper.mapBetDTOToBet(betDTO);
        bet.setCreator(userService.getCurrentUser());
        bet.setOpen(true);
        userService.updateBalanceForBetCreator(userService.getCurrentUser(), betDTO.getBetterAmount(), betDTO.getNumberOfParticipants());
        betRepository.save(bet);

        double betPotSize = betDTO.getBetterAmount() * betDTO.getNumberOfParticipants();
        BetPot betPot = new BetPot(betPotSize, bet, new ArrayList<>());
        betPotRepository.save(betPot);

        return bet;
    }

    //dev Method
    @Transactional
    public Bet userCreatesBet(BetDTO betDTO, BetteryUser betteryUser) {
        Bet bet = new Bet(betDTO.getTitle(), betDTO.getDescription(), betDTO.getNumberOfParticipants(), betDTO.getJoinUntil(), betDTO.getEvaluatedAt(), betDTO.getCategory(),
                betDTO.isOpen(), betDTO.getBetterAmount(),
                betDTO.getJoinerAmount(), betDTO.getWinCondition(),
                betteryUser, LocalDate.now());
        userService.updateBalanceForBetCreator(betteryUser, betDTO.getBetterAmount(), betDTO.getNumberOfParticipants());
        betRepository.save(bet);

        double betPotSize = betDTO.getBetterAmount() * betDTO.getNumberOfParticipants();
        BetPot betPot = new BetPot(betPotSize, bet, new ArrayList<>());
        betPotRepository.save(betPot);

        return bet;
    }

    @Transactional
    public void resolveBet(long betId, boolean posterWon) {
        Bet bet = betRepository.findById(betId).get();
        BetPot betPot = bet.getBetPot();
        BetteryUser creator = bet.getCreator();
        double betPotAmount = betPot.getAmount();
        if (posterWon) {
            userService.setBalanceForUserAfterWin(creator, betPotAmount);
            betPotRepository.save(betPot);
        } else {
            double betterAmount = bet.getBetterAmount();
            double joinerAmount = bet.getJoinerAmount();
            Collection<BetteryUser> joinees = betPot.getUsers();
            for (BetteryUser user : joinees) {
                userService.setBalanceForUserAfterWin(user, betterAmount + joinerAmount);
                betPotAmount -= betterAmount + joinerAmount;
            }
            userService.setBalanceForUserAfterWin(creator, betPotAmount);
        }
        betPot.setAmount(0);
        betPotRepository.save(betPot);
    }

    @Transactional
    public Collection<BetDTO> getBetsThatNeedEvaluation() {
        List<Bet> bets = getAllBets();
        List<Bet> betsThatNeedEvaluation = new ArrayList<>();
        for (Bet bet : bets) {
            if (bet.getEvaluatedAt().isBefore(LocalDate.now()) && bet.getBetPot().getAmount() > 0) {
                betsThatNeedEvaluation.add(bet);
            }
        }
        return betMapper.mapBetListToBetDTOList(betsThatNeedEvaluation);
    }

    @Transactional
    public void closeBetsIfJoinDatePassed() {
        Collection<Bet> bets = betRepository.findAll();
        for (Bet bet : bets) {
            if (bet.getJoinUntil().isBefore(LocalDate.now())) {
                bet.setOpen(false);
                betRepository.save(bet);
            }
        }
    }

    @Transactional
    public boolean isUserPartOfBet(long betID, long userID) {
        Optional<Bet> optionalBet = betRepository.findById(betID);
        Optional<BetteryUser> optionalBetteryUser = userRepository.findById(userID);
        if (optionalBet.isPresent() && optionalBetteryUser.isPresent()) {
            Bet bet = optionalBet.get();
            Collection<BetteryUserDTO> joinees = getJoineesForBet(betID);
            List<Long> idList = joinees.stream().map(betteryUserDTO ->
                    betteryUserDTO.getUserId()
            ).collect(Collectors.toList());
            long creatorId = bet.getCreator().getUserId();
            return idList.contains(userID) || userID == creatorId;
        } else {
            throw new CustomException("User or bet doesn't exist", HttpStatus.NO_CONTENT);
        }
    }
}
