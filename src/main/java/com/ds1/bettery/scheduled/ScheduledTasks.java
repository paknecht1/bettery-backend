package com.ds1.bettery.scheduled;

import com.ds1.bettery.service.BetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduledTasks {

    private final BetService betService;

    @Autowired
    public ScheduledTasks(BetService betService) {
        this.betService = betService;
    }

    @Scheduled(fixedRate = 21600000)
    public void scheduleFixedRateTask() {
        betService.closeBetsIfJoinDatePassed();
    }


}
