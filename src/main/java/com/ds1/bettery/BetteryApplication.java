package com.ds1.bettery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BetteryApplication {

    public static void main(String[] args) {
        SpringApplication.run(BetteryApplication.class, args);
    }

}
