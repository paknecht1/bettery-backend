package com.ds1.bettery.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "bet")
public class Bet {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bet_id")
    private Long betId;
    private String title;
    private String description;
    private int numberOfParticipants;
    private LocalDate joinUntil;
    private LocalDate evaluatedAt;
    private String category;
    private boolean isOpen;
    private double betterAmount;
    private double joinerAmount;
    private String winCondition;
    private LocalDate creationDate;

    @OneToOne(mappedBy = "bet", orphanRemoval = true)
    private BetPot betPot;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bet_user_id")
    private BetteryUser creator;

    public Bet() {

    }

    public Bet(String title, String description, int numberOfParticipants, LocalDate joinUntil, LocalDate evaluatedAt, String category, boolean isOpen, double betterAmount, double joinerAmount, String winCondition, BetteryUser creator, LocalDate creationDate) {
        this.title = title;
        this.description = description;
        this.numberOfParticipants = numberOfParticipants;
        this.joinUntil = joinUntil;
        this.evaluatedAt = evaluatedAt;
        this.category = category;
        this.isOpen = isOpen;
        this.betterAmount = betterAmount;
        this.joinerAmount = joinerAmount;
        this.winCondition = winCondition;
        this.creator = creator;
        this.creationDate = creationDate;
    }

    public Long getBetId() {
        return betId;
    }

    public void setBetId(Long id) {
        this.betId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public void setNumberOfParticipants(int numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public LocalDate getJoinUntil() {
        return joinUntil;
    }

    public void setJoinUntil(LocalDate joinUntil) {
        this.joinUntil = joinUntil;
    }

    public LocalDate getEvaluatedAt() {
        return evaluatedAt;
    }

    public void setEvaluatedAt(LocalDate evaluatedAt) {
        this.evaluatedAt = evaluatedAt;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public double getBetterAmount() {
        return betterAmount;
    }

    public void setBetterAmount(double betterAmount) {
        this.betterAmount = betterAmount;
    }

    public double getJoinerAmount() {
        return joinerAmount;
    }

    public void setJoinerAmount(double joinerAmount) {
        this.joinerAmount = joinerAmount;
    }

    public String getWinCondition() {
        return winCondition;
    }

    public void setWinCondition(String winCondition) {
        this.winCondition = winCondition;
    }

    public BetteryUser getCreator() {
        return creator;
    }

    public void setCreator(BetteryUser creator) {
        this.creator = creator;
    }

    public BetPot getBetPot() {
        return betPot;
    }

    public void setBetPot(BetPot betPot) {
        this.betPot = betPot;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }
}
