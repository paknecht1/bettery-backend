package com.ds1.bettery.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "bettery_user")
public class BetteryUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    private String firstName;

    private String lastName;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private double balance;

    @OneToMany
    @JoinColumn(name="bet_user_id", referencedColumnName="user_id")
    private Collection<Bet> bets = new ArrayList<>();

    @ManyToMany(mappedBy="users", fetch = FetchType.LAZY)
    private Collection<BetPot> betPots = new ArrayList<>();

    @ElementCollection(fetch = FetchType.EAGER)
    List<Role> roles = new ArrayList<>();

    public BetteryUser() {
    }

    public BetteryUser(String firstName, String lastName, String email, String username, String password, List<Role> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public Boolean isAdmin() {
        return roles.stream().map(role -> role == Role.ROLE_ADMIN).findFirst().get();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Collection<Bet> getBets() {
        return bets;
    }

    public void setBets(Collection<Bet> bets) {
        this.bets = bets;
    }

    public Collection<BetPot> getBetPots() {
        return betPots;
    }

    public void setBetPots(Collection<BetPot> betPots) {
        this.betPots = betPots;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
