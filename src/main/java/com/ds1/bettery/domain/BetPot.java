package com.ds1.bettery.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "bet_pot")
public class BetPot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bet_pot_id")
    private Long betPotId;
    private double amount;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bet_bet_id", referencedColumnName = "bet_id")
    private Bet bet;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_bet_pot", joinColumns = { @JoinColumn(name = "bet_pot_id") }, inverseJoinColumns = { @JoinColumn(name = "user_id") })
    private Collection<BetteryUser> users = new ArrayList<>();


    public BetPot() {
    }

    public BetPot(double amount, Bet bet, List<BetteryUser> users) {
        this.amount = amount;
        this.bet = bet;
        this.users = users;
    }

    public Long getBetPotId() {
        return betPotId;
    }

    public void setBetPotId(Long id) {
        this.betPotId = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bets) {
        this.bet = bets;
    }

    public Collection<BetteryUser> getUsers() {
        return users;
    }

    public void setUsers(Collection<BetteryUser> users) {
        this.users = users;
    }
}
