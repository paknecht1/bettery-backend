package com.ds1.bettery.dto.mappers;

import com.ds1.bettery.domain.BetteryUser;
import com.ds1.bettery.dto.BetteryUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper
public interface UserMapper {

    @Mapping(target = "password",
            expression = "java( new String())")
    BetteryUserDTO betteryUserToBetteryUserDTO(BetteryUser user);

    BetteryUser betteryUserDTOToBetteryUser(BetteryUserDTO userDTO);

    Collection<BetteryUserDTO> mapUserListToDTOList(Collection<BetteryUser> betteryUsers);
}
