package com.ds1.bettery.dto.mappers;

import com.ds1.bettery.domain.Bet;
import com.ds1.bettery.dto.BetDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDate;
import java.util.Collection;

@Mapper(imports = {LocalDate.class})
public interface BetMapper {

    @Mapping(target = "creator",
            expression = "java(bet.getCreator().getUserId())")
    BetDTO mapBetToBetDTO(Bet bet);

    @Mapping(target = "creationDate", expression = "java(LocalDate.now())")
    @Mapping(target = "creator", ignore = true)
    Bet mapBetDTOToBet(BetDTO betDTO);

    Collection<BetDTO> mapBetListToBetDTOList(Collection<Bet> betDTO);
}
