package com.ds1.bettery.configuration;

import com.ds1.bettery.domain.Bet;
import com.ds1.bettery.domain.BetteryUser;
import com.ds1.bettery.dto.BetDTO;
import com.ds1.bettery.respository.UserRepository;
import com.ds1.bettery.service.BetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.ds1.bettery.domain.Role.ROLE_ADMIN;
import static com.ds1.bettery.domain.Role.ROLE_CLIENT;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final BetService betService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public DatabaseLoader(UserRepository userRepository, PasswordEncoder passwordEncoder, BetService betService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.betService = betService;
    }

    public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    @Override
    public void run(String... args) throws Exception {

        BetteryUser admin1 = userRepository.findByUsername("admin");
        if (admin1 == null) {
            var admin = new BetteryUser("Admin","Admin", "admin@test.ch", "admin", passwordEncoder.encode("123456"), List.of(ROLE_ADMIN, ROLE_CLIENT));
            var normalUser = new BetteryUser("Normal","User","user@test.ch", "user", passwordEncoder.encode("123456"), List.of(ROLE_CLIENT));
            var fish = new BetteryUser("Fish","Bait","fish@money.ch", "fish", passwordEncoder.encode("fish"), List.of(ROLE_CLIENT));
            var whale = new BetteryUser("Whale","Orca","whale@money.ch", "whale", passwordEncoder.encode("whale"), List.of(ROLE_CLIENT));


            admin.setBalance(10000);
            normalUser.setBalance(10000);
            fish.setBalance(10000);
            whale.setBalance(10000);

            this.userRepository.save(admin);
            this.userRepository.save(normalUser);
            this.userRepository.save(fish);
            this.userRepository.save(whale);

            LocalDate today = LocalDate.now();

            //Bets is open (5days), and doesnt need evaluation yet(10days)
            Bet bet1 = betService.userCreatesBet(new BetDTO("First", "first", 5, today.plusDays(5), today.plusDays(10), "Cat 1", true, 10, 20, "wincon", normalUser.getUserId(), today), normalUser);
            Bet bet2 = betService.userCreatesBet(new BetDTO("First2", "first2", 5, today.plusDays(5), today.plusDays(10), "Cat 2", true, 10, 20, "some wins", normalUser.getUserId(), today), normalUser);
            betService.userCreatesBet(new BetDTO("My Car", "My car will not brake down in the next 10km", 5, today.plusDays(5), today.plusDays(10), "Cars", true, 40, 20, "My car lasts another 10km", fish.getUserId(), today), fish);
            betService.userCreatesBet(new BetDTO("Next battlefield", "I bet the next battlefield will bring back season pass", 5, today.plusDays(5), today.plusDays(10), "Games", true, 30, 30, "The next battlefield has a season pass", whale.getUserId(), today), whale);

            //Bets are closed(2days), dont need evaluation yet(2days)
            Bet bet3 = betService.userCreatesBet(new BetDTO("Football", "The next football match, FCA will win", 5, today.minusDays(2), today.plusDays(2), "Fooball", true, 10, 20, "FCA wins", fish.getUserId(), today.minusDays(4)), fish);
            Bet bet4 = betService.userCreatesBet(new BetDTO("CS Match", "I will ace 2 times next cs match", 3, today.minusDays(2), today.plusDays(2), "CSGO", true, 50, 100, "I ace two times", whale.getUserId(), today.minusDays(4)), whale);

            //Bet is closed, and needs evaluation
            betService.userCreatesBet(new BetDTO("Fast an furious", "I bet the next one will not have any races", 10, today.minusDays(4), today.minusDays(2), "Movies", true, 100, 10, "The movie contains no races", normalUser.getUserId(), today.minusDays(10)), normalUser);
            betService.userCreatesBet(new BetDTO("China", "China will invade taiwan in the next 2 years", 4, today.minusDays(4), today.minusDays(2), "Politics", true, 10, 10, "The world will be nuked", normalUser.getUserId(), today.minusDays(10)), normalUser);
            betService.userCreatesBet(new BetDTO("Biden", "I bet biden will not complete his first term", 10, today.minusDays(4), today.minusDays(2), "Politics", true, 100, 10, "Kamala will take over the white house", normalUser.getUserId(), today.minusDays(10)), normalUser);

            betService.userJoinsBet(bet1.getBetId(), fish);
            betService.userJoinsBet(bet1.getBetId(), whale);

            betService.userJoinsBet(bet2.getBetId(), fish);
            betService.userJoinsBet(bet2.getBetId(), whale);

            betService.userJoinsBet(bet3.getBetId(), normalUser);
            betService.userJoinsBet(bet4.getBetId(), normalUser);
        }
    }
}
