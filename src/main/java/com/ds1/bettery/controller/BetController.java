package com.ds1.bettery.controller;

import com.ds1.bettery.dto.BetDTO;
import com.ds1.bettery.dto.BetteryUserDTO;
import com.ds1.bettery.dto.mappers.BetMapper;
import com.ds1.bettery.service.BetService;
import com.ds1.bettery.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/bets", produces = MediaType.APPLICATION_JSON_VALUE)
public class BetController {

    private BetMapper betMapper = Mappers.getMapper(BetMapper.class);

    private BetService betService;
    private UserService userService;

    @Autowired
    public BetController(BetService betService, UserService userService) {
        this.betService = betService;
        this.userService = userService;
    }

    @GetMapping("/list")
    @ApiOperation(value = "Get a list of all the bets")
    public ResponseEntity<Collection<BetDTO>> getBetsWithJoinableBoolean() {
        Collection<BetDTO> betDTOS = betService.getAllBetsWithJoinableField();
        return ResponseEntity.ok(betDTOS);
    }

    @GetMapping("/evaluate/list")
    @ApiOperation(value = "Get a list of bets where current date has passed the evaluation date")
    public ResponseEntity<Collection<BetDTO>> getBetsThatNeedToBeEvaluated() {
        return ResponseEntity.ok(betService.getBetsThatNeedEvaluation());
    }

    @GetMapping("/{betID}")
    @ApiOperation(value = "Get a bet object that matches the given ID")
    public ResponseEntity<BetDTO> getBet(@PathVariable("betID") long betId) {
        BetDTO bet = betService.getBetDTOById(betId);
        return ResponseEntity.ok(bet);
    }

    @GetMapping("/{betID}/joinees")
    @ApiOperation(value = "Get users that joined the bet of the given bet ID")
    public ResponseEntity<Collection<BetteryUserDTO>> getJoineesOfBet(@PathVariable("betID") long betId) {
        Collection<BetteryUserDTO> betteryUsers = betService.getJoineesForBet(betId);
        return ResponseEntity.ok(betteryUsers);
    }

    @GetMapping("{userID}/ispartof/{betID}")
    @ApiOperation(value = "Check whether user (userID) is part of the bet (betID)")
    public ResponseEntity<Boolean> isUserPartOfBet(@PathVariable("userID") long userId, @PathVariable("betID") long betId) {
        return ResponseEntity.ok(betService.isUserPartOfBet(betId, userId));
    }

    @PutMapping(value = "/{betID}/join")
    @ApiOperation(value = "User currently logged in, joins the given bet")
    public ResponseEntity currentUserJoinsBet(@PathVariable("betID") long betID) {
        betService.currentUserJoinsBet(betID);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/{betID}/delete")
    @ApiOperation(value = "Deletes a bet and it's associations permanently")
    public ResponseEntity delete(@PathVariable("betID") long betID) {
        betService.deleteById(betID);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/create")
    @ApiOperation(value = "Create a bet from the data given in the request")
    public ResponseEntity createNewBet(@RequestBody BetDTO newBet) {
        betService.currentUserCreatesBet(newBet);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/resolve/{betID}/{posterWon}")
    @ApiOperation(value = "Resolving a bet and paying the winners accordingly. If creator won, he gets all the payout. Else it is split between joinees")
    public ResponseEntity resolveBet(@PathVariable("betID") long betID, @PathVariable("posterWon") boolean posterWon) {
        betService.resolveBet(betID, posterWon);
        return ResponseEntity.ok().build();
    }

}
