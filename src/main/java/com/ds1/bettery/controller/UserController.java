package com.ds1.bettery.controller;

import com.ds1.bettery.domain.BetteryUser;
import com.ds1.bettery.domain.Role;
import com.ds1.bettery.dto.BetDTO;
import com.ds1.bettery.dto.BetteryUserDTO;
import com.ds1.bettery.dto.mappers.BetMapper;
import com.ds1.bettery.dto.mappers.UserMapper;
import com.ds1.bettery.exception.CustomValidationException;
import com.ds1.bettery.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private UserService userService;

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    private BetMapper betMapper = Mappers.getMapper(BetMapper.class);

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/me")
    @ApiOperation(value = "Me. Get Information of the current logged in user")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Current User not found")})
    public ResponseEntity<BetteryUserDTO> getUser() {
        BetteryUser user = userService.getCurrentUser();
        return new ResponseEntity<>(userMapper.betteryUserToBetteryUserDTO(user), HttpStatus.OK);
    }

    @GetMapping("/loggedIn")
    @ApiOperation(value = "Check if you are already logged in")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Unauthorized") })
    public ResponseEntity<Boolean> isLoggedIn() {
        return ResponseEntity.ok(true);
    }

    @PostMapping("/login")
    @ApiOperation(value = "Method to call if you want to login")
    @ApiResponses(value = {
            @ApiResponse(code = 422, message = "Invalid username/password supplied")})
    public String login(@RequestParam String username, @RequestParam String password) {
        return userService.login(username, password);
    }

    @PostMapping("/register")
    @ApiOperation(value = "Method to call if you want to register")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid data supplied for registration")})
    public String signUp(@Valid @RequestBody BetteryUserDTO userDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new CustomValidationException("Validation failed", HttpStatus.BAD_REQUEST, errors.getFieldErrors());
        }
        BetteryUser newUser = userMapper.betteryUserDTOToBetteryUser(userDTO);
        newUser.setRoles(List.of(Role.ROLE_CLIENT));
        newUser.setBalance(1000);
        return userService.register(newUser);
    }

    @GetMapping("/bets")
    @ApiOperation(value = "Get All Bets of the currently logged in user")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "User has no bets"),
            @ApiResponse(code = 401, message = "Unauthorized")})
    public ResponseEntity<Collection<BetDTO>> getBetsOfUser() {
        var bets = userService.getBetsOfUser(userService.getUsernameFromCurrentUser());
        if (!bets.isEmpty()) {
            return ResponseEntity.ok(betMapper.mapBetListToBetDTOList(bets));
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
