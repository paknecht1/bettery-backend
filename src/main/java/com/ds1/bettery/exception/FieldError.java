package com.ds1.bettery.exception;

public class FieldError {
    private String message;
    private String field;

    public FieldError(String message, String field) {
        this.message = message;
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public String getField() {
        return field;
    }
}
