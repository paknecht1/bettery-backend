package com.ds1.bettery.exception;

import java.util.List;

public class ErrorMessage {

    private String httpCode;
    private String message;
    private List<FieldError> fieldError;

    public ErrorMessage(String httpCode, String message, List<FieldError> fieldError) {
        this.httpCode = httpCode;
        this.message = message;
        this.fieldError = fieldError;
    }

    public String getHttpCode() {
        return httpCode;
    }

    public String getMessage() {
        return message;
    }

    public List<FieldError> getFieldError() {
        return fieldError;
    }
}
