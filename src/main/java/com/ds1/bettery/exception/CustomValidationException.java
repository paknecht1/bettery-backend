package com.ds1.bettery.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;

public class CustomValidationException extends RuntimeException {

    private static final long serialVersionUID = 2L;

    private final String message;
    private final HttpStatus httpStatus;
    private final List<FieldError> fieldErrors;

    public CustomValidationException(String message, HttpStatus httpStatus, List<FieldError> fieldErrors) {
        this.message = message;
        this.httpStatus = httpStatus;
        this.fieldErrors = fieldErrors;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }
}
