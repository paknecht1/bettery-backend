package com.ds1.bettery.common;


import com.ds1.bettery.exception.FieldError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Util {

    private Util(){
        // hide public constructor
    }

    public static List<FieldError> toListOfInternalFieldError(List<org.springframework.validation.FieldError> fieldErrors) {
        if (fieldErrors.isEmpty()) return Collections.emptyList();
        List<FieldError> internalFieldErrors = new ArrayList<>();
        for (org.springframework.validation.FieldError fieldError : fieldErrors) {
            internalFieldErrors.add(new FieldError(fieldError.getDefaultMessage(), fieldError.getField()));
        }
        return internalFieldErrors;
    }
}
