package com.ds1.bettery.respository;

import com.ds1.bettery.domain.BetteryUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<BetteryUser, Long> {

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    BetteryUser findByUsername(String username);

    void deleteByUsername(String username);

}
