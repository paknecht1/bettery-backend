package com.ds1.bettery.respository;

import com.ds1.bettery.domain.BetPot;
import org.springframework.data.repository.CrudRepository;

public interface BetPotRepository extends CrudRepository<BetPot, Long> {
}
