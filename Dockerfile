FROM openjdk:15-jdk-alpine
RUN addgroup -S backend && adduser -S backend -G backend
RUN apk add curl
USER backend:backend
COPY build/libs/bettery-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]